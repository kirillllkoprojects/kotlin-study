package basics

fun main(args: Array<String>) {
    val list = (1..100).toList()
    println(list.filter { el -> el % 2 == 0 }.toList())
    println(list.filter { it.even() }.toList())
    println(list.filter(::isEven).toList())

}

fun Int.even() = this % 2 == 0

fun isEven(i: Int) = i % 2 == 0