package basics

import java.io.File

fun main(args: Array<String>) {

//    scoping
    File("example.txt").bufferedReader().let {
        if (it.ready()) {
            println(it.readLine())
        }
    }

//    reader should not be visible

    val str: String? = "Kotlin fo Android"
    str?.let{
        if(str.isNotEmpty()){
            str.toLowerCase()
        }
    }
}