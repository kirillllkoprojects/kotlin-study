package basics

fun main(args: Array<String>) {
//    map
    val list = (1..100).toList()
    val mappedList = list.map { it * 2 }.toList()
    println(mappedList)
//    flatMap
    val list2 = listOf(
            (1..10).toList(),
            (11..20).toList(),
            (21..30).toList()
    )
    val notFlattened = list2.map { it }
    val flattened = list2.flatMap { it }
    println(notFlattened)
    println(flattened)

}