package basics

import java.util.*

fun main(args: Array<String>) {
    var i = 0
    val listInt = mutableListOf<Int>()
    val rand = Random()

    while (i < 100) {
        listInt.add(rand.nextInt(50) + 1)
        i++
    }
    println(concat(separator = ",", texts = listOf("Java", "Java", "Kotlin")))
    print(listInt.filter { number -> number <= 10 }.toList())

}

fun concat(texts: List<String>, separator: String = "; ") = texts.joinToString(separator)