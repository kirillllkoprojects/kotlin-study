package basics

fun main(args: Array<String>) {
    val list = generateSequence(0) { it + 10 }

    val first10 = list.take(10).toList()
    val withoutFirst900 = list.drop(900)
    val first20 = list.take(20).toList()

    println(first10)
    println(first20)

}