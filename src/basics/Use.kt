package basics

import java.io.FileReader

fun main(args: Array<String>) {
    FileReader("test.txt").use {
        val str = it.readText()
        println(str)
    }
}