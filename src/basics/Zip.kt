package basics

fun main(args: Array<String>) {
    val list = listOf("Hi", "there", "kotlin", "fans")
    val containsT = listOf(false, true, true, false)

    val mapping = list.zip(list.map { it.contains("t") })

    println(mapping)
}