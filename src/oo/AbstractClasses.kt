package oo

abstract class Shape1(val name: String) {
    abstract fun area(): Double
}

class Circle1(name: String, private val radius: Double) : Shape1(name) {
    override fun area() = Math.PI * Math.pow(radius, 2.0)
}

fun main(args: Array<String>) {
    val smallCircle = Circle1("Large Circle", 17.0)
    println(smallCircle.name)
    println(smallCircle.area())
}