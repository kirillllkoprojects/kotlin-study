package oo

class House(val roomsNumber: Int, val price: Double) {
    companion object {
        val HOUSES_FOR_SALE = 10
        fun getNormalHouse() = House(6, 199999.0)
        fun getLuxuryHouse() = House(42, 7199999.0)
    }
}

fun main(args: Array<String>) {
//    static methods analog
    val luxuryHouse = House.getLuxuryHouse()
    println(luxuryHouse.price)
}