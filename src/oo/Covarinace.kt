package oo

open class Being
open class Person : Being()

class Student : Person()

fun main(args: Array<String>) {

    //covariance = we can use a subtype
    val people: MutableList<Person> = mutableListOf(Person(), Person())
    people.add(Student()) // covariance
    people.add(Person())

}
