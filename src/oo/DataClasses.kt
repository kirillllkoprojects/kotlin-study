package oo

data class Address(val street: String, val number: Int, val postCode: String, val city: String)

fun main(args: Array<String>) {
    val residence = Address("Main Street", 42, "1234", "New York")
    val residence2 = Address("Main Street", 42, "1234", "New York")
//    toString()
    println(residence)
//    referential equality
    println(residence === residence2)
//    structural equality, equals
    println(residence == residence2)

    val neighbor = residence.copy(number = 43)
    println(neighbor)

    val (street, number, postCode, city) = residence
    println("$street $number, $postCode, $city")
}