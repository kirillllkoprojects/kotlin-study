package oo

fun Int.isEven() = (this % 2 == 0)

fun City.isLarge() = population > 1_000_000

fun main(args: Array<String>) {
    println(5.isEven())

    val naturals = listOf(2, 5, 7, 11, 2)
    println(naturals.filter { it.isEven() })

    val austin = City()
    austin.name = "austin"
    austin.population = 950_000
    println(austin.isLarge())
}