package oo

open class Shape(open val name: String) {
    open fun area() = 0.0
}

data class Circle(override val name: String, private val radius: Double) : Shape(name) {
    override fun area() = Math.PI * Math.pow(radius, 2.0)
}

fun main(args: Array<String>) {
    val smallCircle = Circle("Small Circle", 2.0)
    println(smallCircle)
    println(smallCircle.area())
}