package oo

interface Drivable {
    fun drive(){
        println("Drive(Interface)")
    }
}

class Bicycle : Drivable {
    override fun drive() {
        println("Driving bicycle")
    }
}

class Car : Drivable {
    override fun drive() {
        println("Driving car")
    }
}

fun main(args: Array<String>) {
    val drivable: Drivable = Bicycle()
    val drivableCar: Drivable = Car()
    drivable.drive()
    drivableCar.drive()
}