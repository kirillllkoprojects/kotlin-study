package oo

import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

//singleton
object CountryFactory{
    const val a = 4
//    similar to static Java
    fun createCountry() = Country("Australia")
}

object DefaultClickListener: MouseAdapter(){
    override fun mouseClicked(e: MouseEvent?) {
        println("Mouse was clicked")
    }
}

fun main(args: Array<String>) {
    CountryFactory.a
    CountryFactory.createCountry()
}