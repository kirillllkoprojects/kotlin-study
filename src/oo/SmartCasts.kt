package oo

fun Bicycle.replaceWheel() {
    println("Replacing wheel")
}

fun Car.startEngine(): Boolean {
    println("Starting engine...")
    return true
}

fun main(args: Array<String>) {
    val vehicle: Drivable = Bicycle()
//    instance of <-> is
    if (vehicle is Bicycle) {
        vehicle.replaceWheel()
    } else if (vehicle is Car) {
        vehicle.startEngine()
    }
    if (vehicle is Car && vehicle.startEngine()) {
//        do something
    }
    if (vehicle !is Car || vehicle.startEngine()) {
//
    }
    if (vehicle !is Bicycle) {
        return
    }
    vehicle.replaceWheel()
}