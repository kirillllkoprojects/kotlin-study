package oo

// private - same in Java
// protected - same in Java
// internal - visible inside the same module
// default is public (not package-private)

class Train(val brand: String, private val model: String) {
    internal fun tellMeYourModel() = model
}

fun main(args: Array<String>) {
    val train = Train("BRAND", "model")
    train.brand
    train.tellMeYourModel()
}